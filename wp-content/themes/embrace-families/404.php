<?php get_header(); ?>
<h1>Uh oh! The page or file at this address doesn't exist.</h1>
<h5>Maybe try starting over from the <a href="/">home page</a> or searching.</h5>
<?php get_footer(); ?>
