<?php 
  /**
  * Template Name: Workouts
  **/ 
  get_header();

  $image = get_field('video_placeholder_image');
  $workout_programs = get_field('workout_programs');
  $run_time = get_field( 'run_time' );
  $equipment = get_field( 'equipment_used' );
  $title = get_field('heading');
?>

<div class="series-individual pt-2 pb-2 bg-gray">
  <div class="container pt-2 pb-2">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <h2 class="underline color-salmon"><?php the_field('title'); ?></h2>
        <div class="video-player mobile  data-video-id="<?php the_field('video_id'); ?>" style="background-image: url(<?php echo $image['url']; ?>);">
          <div>
            <i class="fa fa-play"></i>
          </div>
        </div>
        <p><?php the_field('content'); ?></p>
      </div>
      <div class="col-sm-12 col-md-5 col-md-offset-1">
        <div class="video-player desktop"  data-video-id="<?php the_field('video_id'); ?>" style="background-image: url(<?php echo $image['url']; ?>);">
            <div>
              <i class="fa fa-play"></i>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col-sm-12">
      <div class="program__wrap">

      <?php 
        if( $workout_programs ):
          foreach( $workout_programs as $post):
              setup_postdata($post);
              $user_id = get_current_user_id();
              $purchased = has_bought_items($user_id, $post->ID);
              $headline = get_field('headline');
              $runtime = get_field('runtime');
              $related_workout = get_field('related_workout');
              foreach( $related_workout as $p ):
                $link_url = get_permalink($p->ID);
              endforeach; 
          ?>

            <a href="<?php echo $link_url; ?>" class="program__individual">
              <div class="program__status">
                <?php if ($purchased) { ?>
                  <i class="fa fa-unlock"></i>
                <? } else { ?> 
                  <i class="fa fa-lock"></i>
                <?php } ?>
              </div>
              <div class="program__title">
                <div>
                  <?php echo $headline; ?>
                </div>
              </div>
              <div class="program__cta">
                <span><?php echo $runtime; ?></span>
                <?php if ($purchased) { ?>
                  <i class="fa fa-play"></i>
                <? } else { ?>
                  <i class="fa fa-angle-right"></i>
                <?php } ?>
              </div>
            </a>

          <?php endforeach; wp_reset_postdata(); ?>
        <?php endif; ?>

      </div>

    </div>
  </div>  
</div>


<?php get_footer(); ?>
