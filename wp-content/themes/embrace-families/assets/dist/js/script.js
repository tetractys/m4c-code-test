// Trigger CSS animations on scroll.
// Detailed explanation can be found at http://www.bram.us/2013/11/20/scroll-animations/

// Looking for a version that also reverses the animation when
// elements scroll below the fold again?
// --> Check https://codepen.io/bramus/pen/vKpjNP

jQuery(function ($) {

		// Function which adds the 'animated' class to any '.animatable' in view
		var doAnimations = function () {

				// Calc current offset and get all animatables
				var offset = $(window).scrollTop() + $(window).height(),
				    $animatables = $('.animatable');

				// Unbind scroll handler if we have no animatables
				if ($animatables.length == 0) {
						$(window).off('scroll', doAnimations);
				}

				// Check all animatables and animate them if necessary
				$animatables.each(function (i) {
						var $animatable = $(this);
						if ($animatable.offset().top + $animatable.height() - 20 < offset) {
								$animatable.removeClass('animatable').addClass('animated');
						}
				});
		};

		// Hook doAnimations on scroll, and trigger a scroll
		$(window).on('scroll', doAnimations);
		$(window).trigger('scroll');
});

(function ($) {
		"use strict";

		var embraceJS = {
				default: {
						test: function () {
								return false;
						},
						run: function () {}
				},

				mobileNavigation: {
						test: function () {
								return true;
						},
						run: function () {
								$('.nav__button').click(function () {
										$('.nav--mobile').toggleClass('active');
										$(this).toggleClass('active');
								});
						}
				},

				videoModule: {
						test: function () {
								return false;
						},
						run: function () {}
				},

				accordion: {
						test: function () {
								return false;
						},
						run: function () {
								$('.accordion .accordion__item').click(function () {

										if ($(this).hasClass('not-active')) {

												$(this).toggleClass('active not-active').siblings().removeClass('active').addClass('not-active');
										} else if ($(this).hasClass('active')) {

												$(this).removeClass('active').siblings().removeClass('not-active');
										} else {
												$(this).toggleClass('active').siblings().toggleClass('not-active');
										}
								});
						}
				},

				slickSlider: {
						test: function () {
								return false;
						},
						run: function () {
								$('.slider-class').slick({
										arrows: true,
										fade: true,
										nextArrow: '<i class="fa fa-angle-right"></i>',
										prevArrow: '<i class="fa fa-angle-left"></i>',
										responsive: [{
												breakpoint: 768,
												settings: {
														slidesToShow: 1,
														slidesToScroll: 1,
														autoplay: true,
														arrows: false
												}
										}]
								});
						}
				},

				mobileAccordion: {
						test: function () {
								if ($('.accordion--mobile').length) {
										return true;
								} else {
										return false;
								}
						},
						run: function () {
								$('.accordion--mobile .accordion__item').click(function () {

										if ($(this).hasClass('collapsed')) {

												$(this).toggleClass('active collapsed').siblings().removeClass('active').addClass('collapsed');
										} else if ($(this).hasClass('active')) {

												$(this).removeClass('active').addClass('collapsed').siblings().addClass('collapsed');
										} else {
												$(this).toggleClass('active').siblings().toggleClass('collapsed');
										}
								});
						}
				},

				statCircleCount: {
						test: function () {
								return $('.stat-circle__single').length;
						},
						run: function () {
								$('.count').each(function () {
										$(this).prop('Counter', 0).animate({
												Counter: $(this).text()
										}, {
												duration: 3000,
												easing: 'swing',
												step: function (now) {
														$(this).text(Math.ceil(now));
												}
										});
								});
						}
				}

		};

		function runObject() {
				for (var key in embraceJS) {
						if (embraceJS[key].test()) {
								embraceJS[key].run();
						}
				}
		}

		runObject();
})(jQuery, window);