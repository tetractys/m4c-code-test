
		</main>
		<footer>
			<div class="footer__brand col-sm-12 col-md-3">
				<img class="footer__logo" src="<?= get_template_directory_uri(); ?>/assets/img/Parent-full-color-tag-horizontal.png" alt="Embrace Families Logo">
			</div>
			<nav class="footer__nav col-sm-12 col-md-6">
				<?php
				$menuParameters = array(
					'theme_location' => 'footer',
					'container'      => false,
					'depth'          => 1,
					'menu_class'		 => 'menu footer__menu',
				);
				echo wp_nav_menu( $menuParameters ) ;
				?>
			</nav>
			<div class="footer__contact-link col-sm-12 col-md-3">
				<a href="/contact">Contact Us</a>
			</div>
		</footer>
		<?php wp_footer(); ?>

	</body>
</html>
