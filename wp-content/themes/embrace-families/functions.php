<?php

if ( ! function_exists( 'embracefamilies_init' ) ) :
	add_action( 'init', 'embracefamilies_init' );

	/**
	* Initialize Scripts
	*/
	function embracefamilies_init() {
		load_theme_textdomain( 'embracefamilies', get_template_directory() . '/languages' );

		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ));
		add_theme_support( 'html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ));

		// Enable Registration of Nav Menus
		register_nav_menus(
			array(
				'primary' 				=> __( 'Primary Menu' ),
				'primary-mobile' 	=> __( 'Primary Mobile Menu' ),
				'footer' 					=> __( 'Footer Menu' )
			)
		);

		/**
		 * Enqueue scripts and styles
		 */
		function embracefamilies_scripts() {
			wp_enqueue_style( 'application-style', get_template_directory_uri() . '/assets/dist/css/app.min.css', true, $package_version );
			wp_enqueue_script( 'application-js', get_template_directory_uri() . '/assets/dist/js/script.js', array( 'jquery' ), $package_version, true );
		}
		add_action( 'wp_enqueue_scripts', 'embracefamilies_scripts' );

		/**
		 * Remove Unnecessary JavaScript in Header
		 */
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_head', 'rsd_link' ); // removes EditURI/RSD (Really Simple Discovery) link.
		remove_action( 'wp_head', 'wlwmanifest_link' ); // removes wlwmanifest (Windows Live Writer) link.
		remove_action( 'wp_head', 'wp_generator' ); // removes meta name generator.
		remove_action( 'wp_head', 'wp_shortlink_wp_head' ); // removes shortlink.
		remove_action( 'wp_head', 'feed_links', 2 ); // removes feed links.
		remove_action( 'wp_head', 'feed_links_extra', 3 );  // removes comments feed.
		remove_action( 'wp_head', 'wp_resource_hints', 2 ); // removes dns-prefetch

		function embracefamilies_deregister_scripts(){
		  wp_deregister_script( 'wp-embed' );
		}
		add_action( 'wp_footer', 'embracefamilies_deregister_scripts' );
	}
endif;

/**
* Miscellaneous Tweaks and Additions
*/

// Require Custom Post Types
require get_template_directory() . '/inc/custom-post-types.php';

// Hide admin bar on front-end
add_filter( 'show_admin_bar', '__return_false' );

/**
 *  Add page slug class to body classes
 */
function embracefamilies_add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'embracefamilies_add_slug_body_class' );
