<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<title><?php wp_title(); ?></title>
  	<meta charset="<?php bloginfo('charset'); ?>" />
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="profile" href="http://gmpg.org/xfn/11" />
  	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<header>

			<a href="/" class="nav__brand" aria-label="logo">
				<img class="nav__logo" src="<?= get_stylesheet_directory_uri(); ?>/assets/img/Parent-full-color-horizontal.png" alt="Embrace Families Logo">
			</a>

			<nav class="header__nav" role="navigation">
			  <div class="nav__button">
				  <div></div>
				  <div></div>
				  <div></div>
				</div>

				<?php
				wp_nav_menu( array(
					'theme_location' 	=> 'primary',
					'container'      	=> 'div',
					'container_class' => 'nav--desktop',
					'depth'          	=> 2,
					'echo'						=> true,
				) );
				?>

				<?php
				wp_nav_menu( array(
					'theme_location' 	=> 'primary-mobile',
					'container'      	=> 'div',
					'container_class' => 'nav--mobile',
					'depth'          	=> 1,
					'echo'						=> true,
				) );
				?>
			</nav>

		</header>

		<main>
