<?php
/**
 * Custom Post Type Template
 */

// function register_staff() {
//   $staff_labels = array(
//     'name'               => 'Staff',
//     'singular_name'      => 'Staff',
//     'add_new'            => 'Add Staff Member',
//     'add_new_item'       => 'Add New Staff Member',
//     'edit_item'          => 'Edit Staff Member',
//     'new_item'           => 'New Staff Member',
//     'all_items'          => 'All Staff Members',
//     'view_item'          => 'View Our Staff',
//     'search_items'       => 'Search Our Staff',
//     'not_found'          => 'No Staff Member found',
//     'not_found_in_trash' => 'No Staff Memeber found in Trash',
//     'parent_item_colon'  => '',
//     'menu_name'          => 'Staff'
//   );

//   $staff_args = array(
//     'labels'             => $staff_labels,
//     'public'             => true,
//     'publicly_queryable' => true,
//     'show_ui'            => true,
//     'show_in_menu'       => true,
//     'query_var'          => true,
//     'rewrite'            => array( 'slug' => 'staff' ),
//     'capability_type'    => 'post',
//     'has_archive'        => false,
//     'hierarchical'       => false,
//     'menu_position'      => null,
//     'supports'           => array( 'title' )
//   );

//   register_post_type( 'staff', $staff_args );
// }
// add_action( 'init', 'register_staff' );
