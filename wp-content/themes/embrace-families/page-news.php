<?php
get_header();

get_template_part( 'partials/acf_hero' );

?>

<div class="container news__container">
  <div class="row">
    <div class="js-guest-post col-sm-12">Please wait while we fetch the latest news for you...</div>
  </div>
</div>

<?php

if ( have_rows( 'content_blocks_flex_field' ) ) : while ( have_rows( 'content_blocks_flex_field' ) ) : the_row();

  switch ( get_row_layout() ) {
    case 'two_column_content':
      get_template_part( 'partials/acf_two_column_content' );
      break;

    case 'key_resources_and_latest_post':
      get_template_part( 'partials/acf_key_resources_and_latest_post');
      break;

    case 'stats_circles':
      get_template_part( 'partials/acf_stats_circles' );
      break;

    case 'donate_cta':
      get_template_part( 'partials/acf_donate_cta' );
      break;

    case 'newsletter_cta':
      get_template_part( 'partials/acf_newsletter_cta' );
      break;

    case 'three_column_content':
      get_template_part( 'partials/acf_three_column_content' );
      break;

    case 'basic_content':
      get_template_part( 'partials/acf_basic_content' );
      break;

    default:
      // I'm afraid I can't do that, Dave.
      break;
  }

endwhile; endif;
?>

<script>

(function() {
  var guestPostColumn = document.querySelector( '.js-guest-post' );

  var getGuestPost = function() {
    fetch( 'https://jsonplaceholder.typicode.com/posts/1' )
      .then( function( res ) {
        return res.json();
      } )
      .then( function( data ) {
        console.log( data );
        let output = '';
        output += `<h2>${data.title}</h2><p>${data.body}</p>`;
        guestPostColumn.innerHTML = output;
      } )
      .catch( function( err ) {
        console.log(err);
      } );
    };

  getGuestPost();

  console.log(guestPostColumn);
}());

</script>

<?php get_footer(); ?>
