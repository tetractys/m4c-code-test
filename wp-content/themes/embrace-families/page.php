<?php
get_header();

get_template_part( 'partials/acf_hero' );

if ( have_rows( 'content_blocks_flex_field' ) ) : while ( have_rows( 'content_blocks_flex_field' ) ) : the_row();

  switch ( get_row_layout() ) {
    case 'two_column_content':
      get_template_part( 'partials/acf_two_column_content' );
      break;

    case 'key_resources_and_latest_post':
      get_template_part( 'partials/acf_key_resources_and_latest_post');
      break;

    case 'stats_circles':
      get_template_part( 'partials/acf_stats_circles' );
      break;

    case 'donate_cta':
      get_template_part( 'partials/acf_donate_cta' );
      break;

    case 'newsletter_cta':
      get_template_part( 'partials/acf_newsletter_cta' );
      break;

    case 'three_column_content':
      get_template_part( 'partials/acf_three_column_content' );
      break;

    case 'basic_content':
      get_template_part( 'partials/acf_basic_content' );
      break;

    default:
      // I'm afraid I can't do that, Dave.
      break;
  }

endwhile; endif;

get_footer();

?>
