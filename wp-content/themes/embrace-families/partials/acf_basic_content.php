<?php

$content = get_sub_field( 'content' );

?>

<div class="container basic-content__container">
  <div class="row">
    <div class="col-sm-12">
      <?= $content; ?>
    </div>
  </div>
</div>
