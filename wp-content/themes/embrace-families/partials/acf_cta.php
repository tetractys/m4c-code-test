<?php
// Content
$main_heading = get_field( 'cta_main_heading' );
$sub_heading 	= get_field( 'cta_sub_heading' );
$button 			= get_field( 'cta_button' );

// Style Options
$main_heading_color = get_field( 'cta_main_heading_color' );
$sub_heading_color 	= get_field( 'cta_sub_heading_color' );
$button_color			 	= get_field( 'cta_button_color' );
?>

<div class="container featured-products__container mt-5 mb-5">
	<div class="row">
		<div class="col-sm-12 text-center">

			<?php if ( $main_heading ) : ?>
			<h4 class="text-upper mb-0 <?= $main_heading_color; ?> animatable fadeInUp"><?= $main_heading; ?></h4>
			<?php endif; ?>

			<?php if ( $sub_heading ) : ?>
			<p class="text-upper mb-0 <?= $sub_heading_color; ?> animatable fadeInUp animationDelay"><?= $sub_heading; ?></p>
			<?php endif; ?>

			<br>

			<a href="<?= $button['url']; ?>" class="btn <?= $button_color; ?> animatable fadeInUp animationDelay"><?= $button['title']; ?></a>

		</div>
	</div>
</div>
