<?php

$button = get_sub_field( 'button' );

?>

<div class="cta__container cta--donate">
  <a href="<?= $button['link_label']['url']; ?>" class="btn btn--<?= $button['button_color']; ?> animatable bounceIn"><?= $button['link_label']['title']; ?></a>
</div>
