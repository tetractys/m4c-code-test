<?php

// Content
$main_heading = get_field( 'main_heading' );
$sub_heading = get_field( 'sub_heading' );
$button = get_field( 'button' );
$background_image = get_field( 'background_image' );

// Style Options
$main_heading_color = get_field( 'main_heading_color' );
$sub_heading_color = get_field( 'sub_heading_color' );
$button_color = get_field( 'button_color' );
$content_position = get_field( 'content_position' );

?>

<div class="hero hero--<?= $content_position; ?>" style="background-image: url( <?= $background_image; ?> );">
	<div class="container">
		<div class="hero__content">

			<?php
			if ( $main_heading ) :
			?>

				<h1 class="hero__headline color-<?= $main_heading_color; ?> animatable bounceInLeft"><?= $main_heading; ?></h1>

			<?php
			endif;
			?>

			<?php
			if ( $sub_heading ) :
			?>

				<p class="hero__subheadline color-<?= $sub_heading_color; ?> animatable bounceInLeft animationDelay"><?= $sub_heading; ?></p>

			<?php
			endif;
			?>
		</div>

		<?php if ( $button ) : ?>
			<a href="<?= $button['url']; ?>" target="<?= $button['target']; ?>" class="btn btn--<?= $button_color; ?> animatable bounceInRight"><?= $button['title']; ?></a>
		<?php endif; ?>
	</div>
</div><!-- /hero -->
