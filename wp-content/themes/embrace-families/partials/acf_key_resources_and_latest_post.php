<?php

$resource_links = get_sub_field( 'resource_links' );
$recent_post = get_sub_field( 'recent_post' );
$counter = 0;
?>
<div class="resources-and-post__wrapper">
  <div class="container resources-and-post__container">
    <div class="row">
      <div class="resources col-sm-12 col-md-8">
        <h2 class="resources__heading color-primary">Key Resources</h2>
        <?php foreach ($resource_links as $resource) : ?>
          <?php $counter++; ?>
          <a class="resources__link animatable bounceInLeft animationDelay--<?= $counter; ?>" href="<?= $resource['link']['url']; ?>" <?php if ( $resource['link']['target'] )  echo $resource['link']['target']; ?>><?= $resource['link']['title'] ?><i class="resources__link__arrow fa fa-angle-right"></i></a>

        <?php endforeach; ?>
      </div>
      <div class="recent-post col-sm-12 col-md-4">
        <?php
        if ( $recent_post ) :
          $post = $recent_post;
          setup_postdata( $post );

          $featured_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        ?>

        <h2 class="recent-post__heading color-primary">What's New</h2>
        <a href="<?= get_permalink(); ?>">
        <div class="recent-post__featured-image" style="background-image: url(<?= $featured_img[0]; ?>);"></div>
        </a>
        <div class="recent-post__body">
          <span class="recent-post__date"><?= get_the_date(); ?></span>
          <h4 class="recent-post__title"><a href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a></h4>
          <p>
            <?= get_the_content( 'Read More' ); ?>
          </p>
        </div>
        <?php
        wp_reset_postdata();
        endif;
        ?>
      </div>
    </div>
  </div>
</div>
