<?php

$heading = get_sub_field( 'heading' );
$subheading = get_sub_field( 'subheading' );

?>

<div class="cta__container cta--newsletter">
  <div class="cta__headings">
    <h2 class="cta__headline animatable fadeInDown"><?= $heading; ?></h2>
    <p class="cta__subheadline animatable fadeInDown animationDelay"><?= $subheading; ?></p>
  </div>
  <div class="cta__form animatable fadeInUp animationDelay">
    <input type="email" class="cta__input--email">
    <a href="#" class="btn btn--orange">Submit</a>
  </div>
</div>
