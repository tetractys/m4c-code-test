<?php

$heading = get_sub_field( 'heading' );
$stat_circles = get_sub_field( 'stat_circles' );
$bottom_text = get_sub_field( 'bottom_text' );

?>

<div class="container stat-circles__section">
  <div class="row">
    <h2 class="stat-circle__heading"><?= $heading; ?></h2>

    <div class="stat-circles__container">
      <?php foreach ( $stat_circles as $stat_circle ) : ?>

        <div class="stat-circle__single">
          <div class="stat-circle__circle stat-circle__circle--<?= $stat_circle['circle_color']; ?>">
            <div class="animatable fadeInUp">
              <span class="stat-circle__number count"><?= $stat_circle['number']; ?></span><span class="stat-circle__number-symbol"><?= $stat_circle['stat_unit_symbol']; ?></span>
            </div>
          </div>
          <h4 class="stat-circle__stat-text animatable fadeInUp animationDelay"><?= $stat_circle['stat_text']; ?></h4>
        </div>

      <?php endforeach; ?>
    </div>

    <p class="stat-circle__bottom-text"><?= $bottom_text; ?></p>
  </div>
</div>
