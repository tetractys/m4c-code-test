<?php

$col1 = get_sub_field( 'column_one' );
$col1_image = $col1['image'];
$col1_heading = $col1['heading'];
$col1_body_text = $col1['body_text'];
$col1_button = $col1['button'];

$col2 = get_sub_field( 'column_two' );
$col2_image = $col2['image'];
$col2_heading = $col2['heading'];
$col2_body_text = $col2['body_text'];
$col2_button = $col2['button'];

$col3 = get_sub_field( 'column_three' );
$col3_image = $col3['image'];
$col3_heading = $col3['heading'];
$col3_body_text = $col3['body_text'];
$col3_button = $col3['button'];

?>

<div class="container three-col__container">
  <h2 class="three-col__heading text-center">Get Involved</h2>
  <div class="row">
    <div class="three-col__column three-col--first col-sm-12 col-md-4">
      <div class="three-col__image" style="background-image: url(<?= $col1_image; ?>);">
        <h4 class="three-col__heading color-white"><?= $col1_heading; ?></h4>
      </div>
      <p class="three-col__body-text color-white"><?= $col1_body_text; ?></p>
      <a href="<?= $col1_button['link_label']['url']; ?>" class="btn btn--<?= $col1_button['button_color']; ?> animatable fadeInUp"><?= $col1_button['link_label']['title']; ?></a>
    </div>
    <div class="three-col__column three-col--second col-sm-12 col-md-4">
      <div class="three-col__image" style="background-image: url(<?= $col2_image; ?>);">
        <h4 class="three-col__heading color-white"><?= $col2_heading; ?></h4>
      </div>
      <p class="three-col__body-text color-white"><?= $col2_body_text; ?></p>
      <a href="<?= $col2_button['link_label']['url']; ?>" class="btn btn--<?= $col2_button['button_color']; ?> animatable fadeInUp"><?= $col2_button['link_label']['title']; ?></a>
    </div>
    <div class="three-col__column three-col--third col-sm-12 col-md-4">
      <div class="three-col__image" style="background-image: url(<?= $col3_image; ?>);">
        <h4 class="three-col__heading color-white"><?= $col3_heading; ?></h4>
      </div>
      <p class="three-col__body-text color-white"><?= $col3_body_text; ?></p>
      <a href="<?= $col3_button['link_label']['url']; ?>" class="btn btn--<?= $col3_button['button_color']; ?> animatable fadeInUp"><?= $col3_button['link_label']['title']; ?></a>
    </div>
  </div>
</div>
