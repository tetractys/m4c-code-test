<?php

$col1 = get_sub_field( 'column_one' );
$col2 = get_sub_field( 'column_two' );

$col1_heading = $col1['heading'];
$col1_body    = $col1['body'];
$col1_button  = $col1['button'];

$col2_heading = $col2['heading'];
$col2_body    = $col2['body'];
$col2_button  = $col2['button'];

?>

<div class="container two-column__container">
  <div class="row">
    <div class="col-sm-12 col-md-6 two-column__col1">
      <h2 class="color-primary"><?= $col1_heading; ?></h2>
      <p><?= $col1_body; ?></p>
      <a href="<?= $col1_button['link_label']['url']; ?>"
        <?php if ( $col1_button['target'] )  echo $col1_button['target']; ?>
        class="btn btn--<?= $col1_button['button_color']; ?>">
        <?= $col1_button['link_label']['title']; ?>
      </a>
    </div>
    <div class="col-sm-12 col-md-6 two-column__col2">
      <h2 class="color-primary"><?= $col2_heading; ?></h2>
      <p><?= $col2_body; ?></p>
      <a href="<?= $col2_button['link_label']['url']; ?>"
        <?php if ( $col1_button['target'] )  echo $col1_button['target']; ?>
        class="btn btn--<?= $col2_button['button_color']; ?>">
        <?= $col2_button['link_label']['title']; ?>
      </a>
    </div>
  </div>
</div>
