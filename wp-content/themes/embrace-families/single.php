<?php
get_header();

get_template_part( 'partials/acf_hero' );

	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h1 class="animatable bounceInLeft"><?php the_title(); ?></h1>
					<p><?php the_content(); ?></p>
				</div>
			</div>
		</div>

	<?php
	endwhile; endif;

get_footer();
?>
